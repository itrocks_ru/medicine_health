<?php

class ItRocks_Form_Decorator_Textarea extends Zend_Form_Decorator_Abstract {

    protected $_format = '
        <textarea name="%s" class="text_input form-control" placeholder="%s" id="%s">%s</textarea>
        %s        
        ';

    protected $_errorFormat = '
        <div class="alert alert-danger">
            <strong>%s</strong>
        </div>
    ';

	public function render($content) {
		$element = $this->getElement();
		$view    = $element->getView();
		if (null === $view) {
			return $content;
		}

        $name = $element->getFullyQualifiedName();
        $label = $element->getLabel();
        $value = $view->escape($element->getValue());
        $id = $element->getAttrib('id');
        $errors = $element->getMessages();


        $markup = sprintf(
            $this->_format,
            $name,
            $view->translate($label),
            $id,
            $value,
            $this->_formatErrors($errors)
        );
        return $markup;
	}

    protected function _formatErrors($errors) {
        $errorString = '';
        foreach ($errors as $error) {
            $errorString .= sprintf($this->_errorFormat, $error);
        }
        return $errorString;
    }
}
