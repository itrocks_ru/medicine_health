<?php

class ItRocks_Form_Decorator_Checkbox extends Zend_Form_Decorator_Abstract {

    protected $_format = '
        <input type="checkbox" id="%s" name="%s" />
        <label for="%s" class="black">%s</label>
        %s        
    ';

    protected $_errorFormat = '
        <div class="alert alert-danger">
            <strong>%s</strong>
        </div>
    ';


    public function render($content) {
        $element = $this->getElement();
        $view = $element->getView();
        if (null === $view) {
            return $content;
        }
        
        $name = $element->getFullyQualifiedName();
        $label = $element->getLabel();
        $id = $element->getId();
        $errors = $element->getMessages();

        $markup = sprintf(
            $this->_format,
            $id,
            $name,
            $id,
            $view->translate($label),
            $this->_formatErrors($errors)
        );
        return $markup;
        
    }

    protected function _formatErrors($errors) {
        $errorString = '';
        foreach ($errors as $key => $error) {
            $errorString .= sprintf($this->_errorFormat, $error);
        }
        return $errorString;
    }
}