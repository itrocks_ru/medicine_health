<?php

class ItRocks_Form_Decorator_Button extends Zend_Form_Decorator_Abstract {

    protected $_format = '
        <button type="submit" class="btn btn_bron" name="%s">%s</button>        
    ';


    public function render($content) {
        $element = $this->getElement();
        $view = $element->getView();
        if (null === $view) {
            return $content;
        }
        
        $name = $element->getFullyQualifiedName();
        $label = $element->getLabel();
        $errors = $element->getMessages();

        $markup = sprintf(
            $this->_format,
            $name,
            $view->translate($label)
        );
        return $markup;
        
    }
}