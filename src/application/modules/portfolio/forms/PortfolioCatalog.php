<?php

class Portfolio_Form_PortfolioCatalog extends Zend_Form {

    protected $_catalog = null;
    
    public function __construct(Zend_Db_Table_Row_Abstract $catalog) {
        $this->_catalog = $catalog;
        parent::__construct();
    }
    
    public function init() {
        $this->setDecorators(array('FormElements', 'Form'));
        $this->setAttrib('class', 'form-horizontal');
        
        $textDecorator = new ItRocks_Form_Decorator_AdminText;
        $buttonDecorator = new ItRocks_Form_Decorator_AdminSubmit;
        
        $this->addElement($this->createElement('text', 'title', array(
            'required' => true,
            'label' => 'portfolioCatalogTitle',
            'value' => $this->_catalog->title,
            'readonly' => false,
            'class' => 'span8',
            'placeholder' => 'Our best service',
            'decorators' => array($textDecorator)
        )));
        
        $this->addElement($this->createElement('submit', 'submit', array (
            'label' => ($this->_catalog->title) ? 'portfolioEditButton' : 'portfolioAddButton',
            'decorators' => array($buttonDecorator)
        )));

    }
}
