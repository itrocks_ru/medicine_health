<?php

class Reviews_AddController extends Zend_Controller_Action{
    
    protected $_reviewsModel;
    
    public function init() {
        $this->_reviewsModel = new Reviews_Model_DbTable_Reviews();        
 
    }
    
    public function indexAction() {
        $form = new Reviews_Form_User();     
        $review = $this->_reviewsModel->fetchNew();

        if ($this->_request->isPost()) {                           
             if ($form->isValid($this->_request->getPost())) {
                 $formData = $this->_request->getPost();
                $review->shortAnswer = $formData['shortAnswer'];
                $review->userLogin = $formData['userLogin'];
                $review->reviewDate = date('Y-m-d H:i:s');
                $review->save();
                    
                $this->_helper->redirector('add-success', 'add', 'reviews');
             }
        }  
        $this->view->form = $form;
    } 

    public function addSuccessAction() {
        
    }
    
}

