<?php


class Zend_View_Helper_NewsImage extends Zend_View_Helper_Abstract {

    public function newsImage($imageName) {
        $imageHelper = new Model_Images_News();
        return $imageHelper->url($imageName);
    }

}