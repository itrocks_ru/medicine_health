<?php

class News_Model_Tags
{
    /**
     * @param $tags
     * @param $articleId
     */
    public function updateTagsByArticle($articleId, $tags){

        $newsTagsTable = new News_Model_DbTable_NewsTags();
        $tagsTable = new Model_DbTable_Tags();

        $wereTags   = $this->getAllTagsFromArticle($articleId, true);
        $tags = $this->clearTagsValue($tags);

        if (!empty($wereTags)){
            $wereTags   = $this->clearTagsValue($wereTags);
            $result     = array_diff($tags, $wereTags);             // get new tags
            $remoteTags = array_diff($wereTags, $tags);             // get remote tags
            $this->removeLinksArticleTags($articleId, $remoteTags);    // remove
        } else{
            $result = $tags;
        }

        foreach($result as $tag)
        {
            $tagFromDb = $tagsTable->searchTag($tag);

            if(empty($tagFromDb))
            {
                //Сначала добавим тег
                $tagId = $tagsTable->addTag($tag);
            }
            else
            {
                $tagId = $tagFromDb['id'];
            }
            $newsTagsTable->insert(['idNews' => $articleId, 'idTags' => $tagId]);
        }
    }
    /**
     * @param $articleId
     * @return string
     */
    public function getAllTagsFromArticle($articleId, $getString = false)
    {
        $tagsTable = new Model_DbTable_Tags();
        $tags = $tagsTable->getTagsByNewsArticle($articleId);
        $return = "";

        if ($getString === true)
        {
            foreach($tags as $tag)
            {
                $return .= $tag['name'].", ";
            }
        } else{
            $return = $tags;
        }
        return $return;
    }


    public function getAllTags()
    {
        $tagsTable = new Model_DbTable_Tags();
        $tags = $tagsTable->getAllNewsTagsSortByCount();
        return $tags;
    }

    private function cmp($a, $b)
    {
        return strnatcmp($a["name"], $b["name"]);
    }

    private function clearTagsValue($tags){
        if (!is_array($tags)){
            $tags = explode(",", $tags);
        }
        foreach ($tags as $tag => $value){
            $tags[$tag] = trim($value);
        }
        $tags = array_diff($tags, array('', ' '));
        $tags = array_unique($tags);
        return $tags;
    }
    private function removeLinksArticleTags($articleId, $remoteTags){
        $newsTagsTable = new News_Model_DbTable_NewsTags();
        $tagsTable = new Model_DbTable_Tags();
        if (!empty($remoteTags)){
            $searchResult = $tagsTable->searchIdbyTagsName($remoteTags);
            $newsTagsTable->removeLinks($articleId,$searchResult);
        }
    }
}