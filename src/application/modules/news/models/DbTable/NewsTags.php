<?php
class News_Model_DbTable_NewsTags extends ItRocks_Db_Table_Abstract
{
    protected $_name = "NewsTags";
    protected $_primary = "idNews";

    public function removeLinks($articleId, $tags){
        $where = [
            'idNews = ?' => $articleId,
            'idTags IN (?)' => $tags
        ];
        $this->delete($where);
    }
}