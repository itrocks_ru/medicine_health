<?php
class Blog_Model_DbTable_BlogTags extends Itrocks_Db_Table_Abstract
{
    protected $_name = "BlogTags";
    protected $_primary = "idBlog";

    public function removeLinks($articleId, $tags){
        $where = [
            'idBlog = ?' => $articleId,
            'idTags IN (?)' => $tags
        ];
        $this->delete($where);
    }
}