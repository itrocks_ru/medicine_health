<?php

class Blog_Form_AdminComment extends ItRocks_Form {

    protected $_comment = null;
    
    public function __construct(Zend_Db_Table_Row_Abstract $comment) {
        $this->_comment = $comment;
        parent::__construct();
    }
    
    public function init() {
        $this->setDecorators(array('FormElements', 'Form'));
        $this->setAttrib('class', 'form-horizontal');
        $this->setAttrib('enctype', 'multipart/form-data');

        $textDecorator = new ItRocks_Form_Decorator_AdminText;
        $textareadDecorator = new ItRocks_Form_Decorator_AdminTextarea;
        $selectDecorator = new ItRocks_Form_Decorator_AdminSelect;
        $buttonDecorator = new ItRocks_Form_Decorator_AdminSubmit;
        
        $this->addElement($this->createElement('text', 'username', array(
            'required' => true,
            'label' => 'blogUsername',
            'value' => $this->_comment->username,
            'readonly' => true,
            'class' => 'span8',
            'placeholder' => '',
            'decorators' => array($textDecorator)
        )));
        
        $this->addElement($this->createElement('text', 'email', array(
            'required' => true,
            'label' => 'blogEmail',
            'value' => $this->_comment->email,
            'readonly' => true,
            'class' => 'span8',
            'placeholder' => '',
            'decorators' => array($textDecorator)
        )));
        
        $this->addElement($this->createElement('select', 'status', array(
            'required' => true,
            'label' => 'blogCommentStatus',
            'value' => $this->_comment->status,
            'multiOptions' => array(
                0 => 'Скрытый',
                1 => 'Видимый'
            ),
            'decorators' => array($selectDecorator)
        )));
        
        $this->addElement($this->createElement('text', 'comment', array(
            'required' => true,
            'label' => 'blogComment',
            'value' => $this->_comment->comment,
            'class' => 'span8',
            'decorators' => array($textareadDecorator)
        )));
        
        $this->addElement($this->createElement('text', 'reply', array(
            'required' => false,
            'label' => 'blogReply',
            'value' => $this->_comment->reply,
            'class' => 'span8',
            'decorators' => array($textareadDecorator)
        )));
        
        $this->addElement($this->createElement('submit', 'submit', array (
            'label' => 'blogEditButton',
            'decorators' => array($buttonDecorator)
        )));
        
    }
}
