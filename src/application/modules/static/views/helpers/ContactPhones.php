<?php


class Zend_View_Helper_ContactPhones extends Zend_View_Helper_Abstract {

    protected $_html = '
        <p class="title_kont">Телефон: <span>%s</span><a href="tel:%s" class="hidden-lg hidden-md">%s</a></p>
    ';

    public function contactPhones($phones) {
        $html = '';
        foreach ($phones as $phone) {
            $html .= sprintf($this->_html, $phone['phone'], $phone['phone'], $phone['phone'], $phone['phone']);
        }
        return $html;
    }

}