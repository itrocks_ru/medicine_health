<?php


class Zend_View_Helper_ServiceImage extends Zend_View_Helper_Abstract {

    public function ServiceImage($imageName) {
        $imageHelper = new Services_Model_Images();
        return $imageHelper->url($imageName);
    }

}