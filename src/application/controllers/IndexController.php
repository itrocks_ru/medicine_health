<?php

class IndexController extends Zend_Controller_Action {

    public function mainMenuAction() {
        $action = $this->_request->getParam('action');
        $alias = $this->_request->getParam('alias');
        $module = $this->_request->getParam('module');
        $controller = $this->_request->getParam('module');

        $modules = Bootstrap::getModuleList();
        $this->view->assign('modules', $modules);

        $this->view->assign('action', $action);
        $this->view->assign('controller', $controller);
        $this->view->assign('module', $module);
        $this->view->assign('alias', $alias);
    }

    public function headerAction() {
        $settingsModel = new Model_Settings();
        $phonesTable = new Admin_Model_DbTable_Phones();

        $data = $settingsModel->getSettings(['address', 'email', 'workTime']);
        $data['phone'] = $phonesTable->getMainPhone();

        $this->view->assign('data', $data);
    }

    public function socialIconsAction() {
        $socialIconsTable = new Admin_Model_DbTable_SocialNetworks();
        $socialIcons = $socialIconsTable->getAll();

        $this->view->assign('social', $socialIcons);
    }

    public function socialIconsMobileAction() {
        $socialIconsTable = new Admin_Model_DbTable_SocialNetworks();
        $socialIcons = $socialIconsTable->getAll();

        $this->view->assign('social', $socialIcons);
    }

    public function footerSocialAction() {
        $socialIconsTable = new Admin_Model_DbTable_SocialNetworks();
        $socialIcons = $socialIconsTable->getAll();

        $this->view->assign('social', $socialIcons);
    }
   
    public function searchAction() {
        $modules = Bootstrap::getModuleList();
        $searchModel = new Model_Search();
        $request = trim($this->getParam('query'));
        $buttonTitle = "buttonSearchTitle";
        $items = [];
        $errorString = "";

        if ($this->_request->isGet()) {
                $buttonTitle = "buttonSearchTryAgain";
                if (strlen($request) > 0) {

                    $items = $searchModel->search($request);
                    if (empty($items)) {
                        $errorString = "resultsNotFound";
                    }
                } else {
                    $errorString = "minimumCharInStringSearch";
                }
        }

        $this->view->assign('modules', $modules);
        $this->view->assign('searchQuery', $request);
        $this->view->assign('searchTitle', $buttonTitle);
        $this->view->assign('items', $items);
        $this->view->assign('errorString', $errorString);
    }

    public function feedbackAction(){
        $diyTable = new Static_Model_DbTable_Feedback();
        $feedback = $diyTable->createRow();
        $form = new Form_Feedback($feedback);

        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            if ($form->isValid($formData)) {
                try {
                    $formData['date'] = date('Y-m-d H:i:s');
                    $feedback->setFromArray($formData);
                    $feedback->save();

                    $mailModel = new Model_Mail();
                    $mailModel->sendRequestFeedbackFormToAdmin($formData, $this->view->baseUrl());

                    $this->_helper->redirector('feedback-success', 'index', 'default');
                } catch (Exception $e) {
                    $this->view->assign('error', $e->getMessage());
                }
            }
        }
        $this->view->assign('form', $form);
    }

    public function feedbackSuccessAction(){

    }

    public function footerAction() {
        $settingsModel = new Model_Settings();
        $phonesTable = new Admin_Model_DbTable_Phones();

        $data = $settingsModel->getSettings(['yandexMetrika', 'googleAnalytics', 'address', 'email']);
        $data['phone'] = $phonesTable->getMainPhone();

        $this->view->assign('data', $data);
    }

    public function sliderAction(){
        $sliderTable = new Static_Model_DbTable_Slider();
        $images = $sliderTable->getSliderImages();
        $this->view->assign('images', $images);
    }

}