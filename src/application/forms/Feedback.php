<?php

class Form_Feedback extends ItRocks_Form  {

    protected $_feedback = null;

    public function __construct(Zend_Db_Table_Row_Abstract $feedback) {
        $this->_feedback     = $feedback;
        parent::__construct();
    }

    public function init() {
        $this->setDecorators(array('FormElements', 'Form'));
        $this->setAttrib('class', 'form-horizontal');
        $this->setAttrib('enctype', 'multipart/form-data');

        $textDecorator      = new ItRocks_Form_Decorator_Text;
        $textareaDecorator = new ItRocks_Form_Decorator_Textarea;
        $buttonDecorator    = new ItRocks_Form_Decorator_Button();
        $checkboxDecorator = new ItRocks_Form_Decorator_Checkbox();

        $this->addElement($this->createElement('text', 'name', array(
            'required' => false,
            'label' => 'ФИО',
            'value' => $this->_feedback->name,
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'phone', array(
            'required' => false,
            'label' => 'Телефон',
            'value' => $this->_feedback->phone,
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'email', array(
            'required' => false,
            'label' => 'E-mail',
            'value' => $this->_feedback->email,
            'decorators' => array($textDecorator)
        )));

        $this->addElement($this->createElement('text', 'message', array(
            'required' => false,
            'label' => 'Задайте вопрос',
            'value' => $this->_feedback->message,
            'decorators' => array($textareaDecorator)
        )));

        $this->addElement($this->createElement('text', 'agreement', array(
            'required' => false,
            'label' => 'Согласие на обработку персональных данных',
            'decorators' => array($checkboxDecorator)
        )));


        $this->addElement($this->createElement('submit', 'submit', array (
            'label' => 'Отправить',
            'decorators' => array($buttonDecorator)
        )));
    }

    public function isValid($data){

        $res = parent::isValid($data);

        if (empty($data['email']) && empty($data['phone'])) {
            $this->email->addError('Укажите телефон или e-mail');
            $this->phone->addError('Укажите телефон или e-mail');
            $res = false;
        }

        if (!isset($data['agreement'])) {
            $this->agreement->addError('Подтвердите согласие на обработку персональных данных');
            $res = false;
        }

        return $res;
    }
}
