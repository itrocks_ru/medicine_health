'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-minify-css'),
    less = require('gulp-less'),
    rigger = require('gulp-rigger'),
    pngquant = require('imagemin-pngquant'),
    imagemin = require('gulp-imagemin');

var path = {
    build: {
        html: 'design/build/',
        js: 'design/build/js/',
        css: 'design/build/css/',
        img: 'design/build/img/',
        fonts: 'design/build/fonts/'
    },
    src: {
        html: 'design/src/*.html',
        js: 'design/src/js/main.js',
        style: 'design/src/css/main.less',
        img: 'design/src/img/**/*.*',
        fonts: 'design/src/fonts/**/*.*'
    },
    watch: {
        html: 'design/src/**/*.html',
        js: 'design/src/js/**/*.js',
        style: 'design/src/css/**/*.less',
        img: 'design/src/img/**/*.*',
        fonts: 'design/src/fonts/**/*.*'
    },
    clean: './build',
    deploy: {
        js: 'src/www/js/',
        css: 'src/www/css/',
        img: 'src/www/img/',
        fonts: 'src/www/fonts/'
    }
};

gulp.task('html:build', function() {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
});

gulp.task('js:deploy', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.deploy.js));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(less())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css));
});

gulp.task('style:deploy', function () {
    gulp.src(path.src.style)
        .pipe(less())
        .pipe(cssmin())
        .pipe(gulp.dest(path.deploy.css));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img));
});

gulp.task('image:deploy', function () {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.deploy.img));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('fonts:deploy', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.deploy.fonts))
});

gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'fonts:build',
    'image:build'
]);

gulp.task('deploy', [
    'js:deploy',
    'style:deploy',
    'fonts:deploy',
    'image:deploy'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('default', ['build', 'watch']);